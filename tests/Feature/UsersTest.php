<?php

use App\Models\User;
use Inertia\Testing\AssertableInertia as Assert;


test('User index can be render', function () {

    $user = User::factory()->create();

    $this->actingAs($user);

    $response = $this->get('/users/');

    $response->assertStatus(200);
});

test('User index can filter by name', function () {
    $user = User::factory()->create();

    $this->actingAs($user);

    $response = $this->get('/users/');

    $response->assertInertia(fn (Assert $page) => $page->has('users'));
});

